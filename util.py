import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import warnings


def replace_with(data, feature_name, value_to_replace, new_value):
    data.loc[data[feature_name] == value_to_replace, feature_name] = new_value
    assert data.loc[data[feature_name] ==
                    value_to_replace, feature_name].shape == (0,)


def completeness(feature):
    nans = feature.isnull().sum()
    return 1 - nans / len(feature)


def count_nans_and_zeros(data, feature_name):
    nans = data[feature_name].isnull().sum()
    zeros = data[data[feature_name] == 0][feature_name].shape[0]

    print('{: <20} NaNs: {: <4} Zeros: {: <4}'
          .format(feature_name, nans, zeros))


def count_unique(data, feature_name):
    unique = data.fillna('NaN_count_unique').groupby(
        feature_name)['Id'].nunique(dropna=False)
    return unique


def ordinal_to_numerical(data, feature_name, order_str):
    map_dict = create_ordinal_mapper(order_str)
    feature = data[feature_name]

    feature = feature.replace(map_dict)
    # NaN values are replaced by 0
    return feature.fillna(0)


def create_ordinal_mapper(items_in_order_desc_str):
    items_in_order_desc = items_in_order_desc_str.split(' ')
    id_counter = len(items_in_order_desc)

    map_dict = {}
    for i in range(len(items_in_order_desc)):
        map_dict[items_in_order_desc[i]] = id_counter
        id_counter -= 1

    return map_dict


def plot_feature(data, x_feature, y_feature):
    plot_data = np.array([data[x_feature].values, data[y_feature].values])

    plt.xlabel(x_feature)
    plt.ylabel(y_feature)
    plt.scatter(x=plot_data[0], y=plot_data[1], marker='.', s=10)


def plot_feature_old(data, feature_name, prices, plot_outliers=False):
    feature = data[feature_name].values
    prices = prices.values
    plot_data = np.array([feature, prices])

    plt.xlabel(feature_name)
    plt.ylabel('Sale Price')
    plt.scatter(x=plot_data[0], y=plot_data[1], marker='.', s=10)

    if not plot_outliers:
        plt.show()
        return

    # Fit a line to the data
    coefs = np.polyfit(x=data[feature_name], y=prices, deg=1)
    # We don't need to find every data point to draw a line,
    # so let's find the max point and plot a line from min to max point
    fitted_line_x = [data[feature_name].min(), data[feature_name].max()]
    fitted_line_y = list(map(lambda x: coefs[0] * x + coefs[1], fitted_line_x))
    plt.plot(fitted_line_x, fitted_line_y, 'black')

    # To detect outliers, let's use square difference between
    # actual prices and prices predicted by the line
    fitted_prices = list(map(lambda x: coefs[0] * x + coefs[1], feature))
    square_diff = np.square(fitted_prices - prices)
    sorted_diff_indices = np.argsort(square_diff)
    # Let's pick N largest distances
    outlier_indices = sorted_diff_indices[-5:]
    square_diff[outlier_indices]
    # Let's plot outliers in a different color
    plt.scatter(x=feature[outlier_indices], y=prices[outlier_indices],
                marker='.', c='red', s=30)
    # Annotate their Ids
    for i in outlier_indices:
        # plt.annotate(data['Id'][i], (feature[i], prices[i]))
        plt.annotate(i, (feature[i], prices[i]))

    plt.show()


def describe_feature(data, feature_name, y, is_categorical=False, plot_outliers=False):
    feature = data[feature_name]
    plot_feature(data, feature_name, y, plot_outliers=plot_outliers)

    warnings.filterwarnings("ignore")
    sns.distplot(feature)
    warnings.resetwarnings()
    plt.show()

    if is_categorical:
        data = pd.concat([feature, y], axis=1)
        fig = sns.boxplot(x=feature, y=y, data=data)


def corr_map(data, features):
    # This is a temporary columns.
    # Later, categorical variables should be added.
    corr_columns = list(features + ['SalePrice'])
    corr_data = data[corr_columns]
    # corr_data = (cov_data - cov_data.mean()) / cov_data.std()
    corr_data = corr_data.corr()

    f, ax = plt.subplots(figsize=(10, 8))
    sns.heatmap(corr_data)

    # Sort the correlation matrix by feature-SalePrice correlation,
    # taking absolute correlation values
    sorted_corr = corr_data.abs().sort_values('SalePrice', axis='rows',
                                              ascending=False)
    # Display the sorted values except SalePrice-SalePrice correlation
    sorted_corr['SalePrice'][1:]

    return sorted_corr['SalePrice'][1:]
